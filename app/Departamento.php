<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    public $table = 'departamento';

    public $primaryKey = 'id';

    public  $fillable = [
        'id',
        'nombre',
        'descripcion',
        'codigo',
        'created_at',
        'updated_at'
    ];
}
