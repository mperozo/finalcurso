<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Producto;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    public function obtener_todos(){
        $departamentos = Departamento::all();
        return $departamentos;

    }

    public function todos(){
        $departamentos = self::obtener_todos();
        return view('departamentos')->with([
            'departamentos' => $departamentos
        ]);
    }

    public function  nuevo(){
        return view('nuevodepartamento');
    }

    public function nuevopost(Request $request){
        //return $request->all();

        $nombre = $request->get('nombre');
        $descripcion = $request->get('descripcion');
        $codigo= $request->get('codigo');

        $departamento = new Departamento();

        $departamento->nombre = $nombre;
        $departamento->codigo = $codigo;
        $departamento->descripcion = $descripcion;
        $departamento->save();

        return redirect()->route('departamento.todos');

   }

   public function todos_json(Request $request) {
       if($request->has ('todos')){
           $departamentos = Departamento::all();
        return $departamentos;
        }
       if($request->has ('nombre')){
           $departamentos = Departamento::where('nombre', 'like', '%'. $request->get('nombre').'%' )->get();
           return $departamentos;
       }
        if($request->has ('descripcion')){
            $departamentos = Departamento::where('descripcion', 'like', '%'. $request->get('descripcion').'%' )->get();
            return $departamentos;
        }

        return [];

   }
}
