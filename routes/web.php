<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/productos', [
    'as' => 'productos.todos',
    'uses' => 'ProductoController@todos'
]);

Route::get('/nuevo_producto', [
    'as' => 'nuevo.producto',
    'uses' => 'ProductoController@nuevo'
]);

Route::get('/api/productos', [
    'as' => 'api.todos.productos',
    'uses' => 'ProductoController@todos_json'
]);

Route::post('/nuevo_producto', [
    'as' => 'nuevo.producto.post',
    'uses' => 'ProductoController@nuevopost'
]);


Route::get('/departamentos', [
    'as' => 'departamento.todos',
    'uses' => 'DepartamentoController@todos'
]);

Route::get('/nuevo_departamento', [
    'as' => 'nuevo.departamento',
    'uses' => 'DepartamentoController@nuevo'
]);

Route::get('/api/departamentos', [
    'as' => 'api.todos.departamentos',
    'uses' => 'DepartamentoController@todos_json'
]);

Route::post('/nuevo_departamento', [
    'as' => 'nuevo.departamento.post',
    'uses' => 'DepartamentoController@nuevopost'
]);