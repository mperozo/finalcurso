@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Todos los Departamentos</div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('nuevo.departamento')}}" class="btn btn-success pull-right">Nuevo Departamento</a>
                    </div>
                </div>
                <div class="panel-body">
                    <h4>Filtrar</h4>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Filtrar por</label>
                            <select name="filtro" id="filtro" class="form-control">
                                <option value="0">Todos</option>
                                <option value="1">Nombre</option>
                                <option value="2">Descripción</option>

                            </select>
                        </div>
                        <div class="col-md-3 nombre" style="display: none">
                            <label for="">Escriba nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre">
                        </div>

                        <div class="col-md-3 descripcion" style="display: none">
                            <label for="">Escriba decsripción</label>
                            <input type="text" class="form-control" id="descripcion" name="description">
                        </div>

                        <div class="col-md-3 ">
                            <a class="btn btn-primary" id="btn-search"> Buscar <i class="icon-search"></i></a>
                        </div>
                    </div>
                    <br>

                    <table class="table table-bordered" style="margin-top: 20px">
                        <thead>
                        <tr>
                            <th>nombre</th>
                            <th>descripcion</th>
                            <th>codigo</th>

                        </tr>
                        </thead>
                        <tbody id="tbody">
                        @foreach($departamentos as $departamento)
                            <tr>
                                <td>{{$departamento->nombre}}</td>
                                <td>{{$departamento->descripcion}}</td>
                                <td>{{$departamento->codigo}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click','#btn-search', function (){
            filtrar($('#filtro').val());
        });

        $(document).on('change','#filtro',function(){
            $this = $(this);
            switch ($this.val()){
                case '0':
                    $('.nombre').hide();
                    $('.descripcion').hide();


                    break;
                case '1':
                    $('.nombre').show();
                    $('.descripcion').hide();


                    break;
                case '2':
                    $('.descripcion').show();
                    $('.nombre').hide();


                    break;

            }
        });

        function filtrar(type){
            $url='{{route('api.todos.departamentos')}}';
            $valid=0;
            console.log(type);
            switch  (parseInt(type)) {
                case 0:
                    $url= $url + '?todos=1';
                    break;
                case 1:
                    if ($('#nombre').val()===''){
                        alert('por favor ingrese nombre');
                        $valid=1;
                    }
                    $url= $url + '?nombre='+ $('#nombre').val();
                    break;
                case 2:
                    if ($('#descripcion').val()===''){
                        alert('por favor ingrese descripcion');
                        $valid=1;
                    }
                    $url= $url + '?descripcion='+ $('#descripcion').val();
                    break;
            }
            if ($valid ===0)
            {
                $.ajax({
                    url: $url,
                    success: function(respuesta){
                         $('#tbody').html('');
                         $.each(respuesta, function (i,v) {
                             $('#tbody').append('<tr>\n'+
                                 '<td>'+v.nombre+'</td>\n' +
                             '<td>'+v.descripcion+'</td>\n' +
                             '<td>'+v.codigo+'</td>\n' +
                             '</tr>'
                             );
                         })

                    },
                    error: function (respuesta) {

                    }

                })
            }
         }
    </script>
@endsection