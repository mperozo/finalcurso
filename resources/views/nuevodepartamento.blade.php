@extends('layouts.app')

@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Nuevo Departamento</div>

                    <div class="panel-body">

                        <form action="{{route('nuevo.departamento.post')}}" method="post">
                            <label for="nombre">Nombre</label>
                            <input required type="text" name="nombre" placeholder="Nombre..." class="form-control">
                            <label for="descipcion">Descripcion</label>
                            <textarea required name="descripcion"  class="form-control" id="" cols="30" rows="10"></textarea>
                            <label for="codigo">Codigo</label>
                            <input required type="text" name="codigo" placeholder="codigo" class="form-control">

                            <button type="submit" class="btn btn-success">Guardar</button>

                        </form>

                    </div>
                </div>
            </div>
        </div>

@endsection
